# Web Dev Intern Challenge

## Description

Simple web app for searching repos and managing favourites. Built with react on
meteor. Demo of app in production is running [here](http://ec2-18-222-237-159.us-east-2.compute.amazonaws.com/).

## Installing

Built with Meteor 1.7. Install with:
```bash
curl https://install.meteor.com/ | sh
```

Clone source into new local directory:
```bash
git clone https://bitbucket.org/beth92/web-dev-intern-challenge/.git
cd web-dev-intern-challenge
```
Install app via Meteor:

```bash
meteor npm install
```

## Authenticating and Running
For security reasons I have not committed my own GitHub credentials. However a valid username and password (or access token) is is required to avoid rate limiting.
Create a file called `auth.js` and place it inside the `./imports/api/` directory. It requires the following contents:

```javascript
export default auth = {
  username: "GITHUB_USERNAME",
  password: "PASSWORD_OR_ACCESS_TOKEN"
};
```
Replacing username and password with valid GitHub credentials. Then run with:
```bash
meteor
```

and visit `localhost:3000` to view app running in browser.

## Notes on Behaviour

- Search results will clear when search input is cleared
- On narrow screens the view will adjust to column layout to save space
- There are no user accounts in this app, however favourited repositories are persisted in SessionStorage
- Search results will be limited to the top 10 repositories
- If search results or favourites list is empty, placeholder message will show in place of repo details
