import React from 'react';
import PropTypes from 'prop-types';

export default class Result extends React.Component {

  addResult(result) {
    result.is_favourite = true;
    const favourites = JSON.parse(window.sessionStorage.getItem('favourites')) || [];
    window.sessionStorage.setItem('favourites', JSON.stringify(favourites.concat(result)));
    this.props.addFavourite(result);
  }

  removeResult(id) {
    const favourites = JSON.parse(window.sessionStorage.getItem('favourites')) || [];
    window.sessionStorage.setItem('favourites', JSON.stringify(favourites.filter((fav) => {
      return fav.id !== id;
    })));
    this.props.removeFavourite(id);
  }

  render() {
    return (
      <tr className="result">
        <td>{ this.props.data.name }</td>
        <td>{ this.props.data.language }</td>
        <td>{ this.props.data.latest_tag }</td>
        <td className="result__action-link" onClick={ () => {
          if (this.props.action === "Add") {
            this.addResult(this.props.data);
          } else if (this.props.action === "Remove") {
            this.removeResult(this.props.data.id);
          }
        } } >{ this.props.action }</td>
      </tr>
    );
  }
}

Result.propTypes ={
  data: PropTypes.exact({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    latest_tag: PropTypes.string.isRequired,
    is_favourite: PropTypes.bool.isRequired
  }),
  addFavourite: PropTypes.func,
  removeFavourite: PropTypes.func,
  action: PropTypes.oneOf(["", "Add", "Remove"])
};
