import React from 'react';
import PropTypes from 'prop-types';

import { getRepos } from '../api/github';

export default class SearchBar extends React.Component {

  constructor () {
    super();
    this.state = {
      buttonText: "Search"
    };
  }

  handleSearchSubmit(e) {
    // prevent page refresh
    e.preventDefault();

    const term = e.target.term.value.trim();
    if (term !== "") {
      this.setState({
        buttonText: "Loading..."
      });
      getRepos(term).then((res) => {
        this.setState({
          buttonText: "Search"
        });
        this.props.updateResults(res);
      }).catch(e => {
          alert(`Unexpected error: ${e}`);
          this.setState({
            buttonText: "Search"
          });
      });
    }
  }

  handleChange(e) {
    // clear search results if input field cleared
    if (e.target.value === "") {
      this.props.updateResults([]);
    }
  }

  render() {
    return (
      <div className='search-bar'>
        <form className="search-bar__form" onSubmit={ this.handleSearchSubmit.bind(this) }>
          <input type="text" name="term" className="search-bar__input" onChange={ this.handleChange.bind(this) } autoComplete="off"/>
          <button className="search-bar__button" disabled = { this.state.buttonText==="Loading..." }>
            { this.state.buttonText }
          </button>
        </form>
      </div>
    );
  }
}

SearchBar.propTypes = {
  updateResults: PropTypes.func.isRequired
};
