import React from 'react';

// components
import Search from './Search';
import Favourites from './Favourites';


export default class MainContent extends React.Component {
  constructor () {
    super();
    this.state = {
      favourites: initFavourites(),
      searchResults: []
    };
  }

  addFavourite(result) {
    this.setState((state) => {
      return {
        favourites: state.favourites.concat(result)
      };
    });
  }

  removeFavourite(id) {
    const favouriteInResults = this.state.searchResults.findIndex((result) => {
      return result.id === id;
    });
    this.setState((state) => {
      const searchResults = state.searchResults;
      if (favouriteInResults >= 0) {
        searchResults[favouriteInResults].is_favourite = false;
      }
      return {
        favourites: state.favourites.filter((fav) => {
          return fav.id !== id;
        }),
        searchResults
      };
    });
  }

  updateResults (newResults) {
    this.setState({
      searchResults: newResults
    });
  }

  render() {
    return (
      <div className='main-content'>
        <Search addFavourite={ this.addFavourite.bind(this) } data={ this.state.searchResults } updateResults={ this.updateResults.bind(this) }/>
        <Favourites removeFavourite={ this.removeFavourite.bind(this) } data={ this.state.favourites }/>
      </div>
    );
  }
}

initFavourites = () => {
  return JSON.parse(window.sessionStorage.getItem('favourites')) || [];
};
