import React from 'react';
import PropTypes from 'prop-types';

import Result from './Result';

export default class SearchResults extends React.Component {
  constructor () {
    super();
  }

  renderResults(results) {
    if(results.length > 0) {
      return results.map((result, index) => {
        if (this.props.type === "search") {
          return <Result data={ result } key={ index } action={ result.is_favourite ? "" : "Add" } addFavourite={ this.props.addFavourite }/>;
        } else if (this.props.type === "favourites" ) {
          return <Result data={ result } key={ index } action="Remove" removeFavourite = { this.props.removeFavourite }/>;
        }
      });
    } else {
      return (
        <tr className="search-results--empty" ><td colSpan="3">No repos to show</td></tr>
      );
    }
  }

  render() {
    return (
      <table className='search-results'>
        <tbody>
          <tr className="search-results__header">
            <th>Name</th>
            <th>Language</th>
            <th>Latest tag</th>
            <th></th>
          </tr>
          { this.renderResults( this.props.data ) }
        </tbody>
      </table>
    );
  }
}


SearchResults.propTypes = {
  type: PropTypes.oneOf(["search", "favourites"]),
  data: PropTypes.array,
  addFavourite: PropTypes.func,
  removeFavourite: PropTypes.func
};
