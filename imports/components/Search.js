import React from 'react';
import PropTypes from 'prop-types';

// components
import SearchBar from './SearchBar';
import SearchResults from './SearchResults';

export default class Search extends React.Component {

  render() {
    return (
      <div className='search'>
        <SearchBar updateResults={ this.props.updateResults }/>
        <SearchResults type="search" addFavourite={ this.props.addFavourite } data={ this.props.data }/>
      </div>
    );
  }
}

Search.propTypes = {
  addFavourite: PropTypes.func.isRequired,
  updateResults: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired
};
