import React from 'react';
import PropTypes from 'prop-types';

import SearchResults from './SearchResults';

export default class Favourites extends React.Component {
  render() {
    return (
      <div className='favourites' >
        <SearchResults type="favourites" removeFavourite={ this.props.removeFavourite } data={ this.props.data }/>
      </div>
    );
  }
}

Favourites.propTypes = {
  removeFavourite: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired
};
