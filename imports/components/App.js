import React from 'react';

// components
import TitleBar from './TitleBar';
import MainContent from './MainContent';

export default class App extends React.Component {

  render() {
    return (
      <React.Fragment>
        <TitleBar title="My GitHub Favorites"/>
        <MainContent/>
      </React.Fragment>
    );
  }
}
