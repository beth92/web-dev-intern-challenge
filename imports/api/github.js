const axios = require('axios');

try {
  import auth from './auth';
} catch (e) {
  console.log("No GitHub credentials found, running unauthenticated");
  auth = {};
}

// fetch repos using API given search term
export const getRepos = async (term) => {
  const repos = await axios.request({
    method: "GET",
    url: `https://api.github.com/search/repositories?q=${term}`,
    auth
  });
  if (repos.status === 200 && repos.data.total_count > 0) {
    const resultsPromises = await repos.data.items.slice(0, 10).map( async (repo) => {
      const tag = await getTag(repo.tags_url);
      return {
        id: repo.id,
        "name": repo.full_name,
        "language": repo.language,
        "latest_tag": tag,
        "is_favourite": checkFavourite(repo.id)
      }
    });
    return Promise.all(resultsPromises);
  } else if (repos.data.total_count === 0) {
    return [];
  } else {
    throw new Error(repos.status);
  }
};

const getTag = async (tags_url) => {
  const res = await axios.request({
    method: "GET",
    url: tags_url,
    auth
  });
  if (res.status === 200 && res.data.length) {
    return res.data[0].name;
  } else {
    return "";
  }
};

const checkFavourite = (id) => {
  const favourites = JSON.parse(window.sessionStorage.getItem('favourites')) || [];
  return favourites.some((fav) => {
    return fav.id === id;
  });
};
